#ifndef POWERMENUENTRYCREATOR_H
#define POWERMENUENTRYCREATOR_H

#include <QObject>
#include <QString>
#include <QtXml/QXmlStreamWriter>
#include <QFileInfo>

#include "DesktopFileScanner.h"


class PowerMenuEntryCreator : public QObject
{
    Q_OBJECT
    Q_ENUMS(EntryType)
public:
    Q_PROPERTY(int priority READ getPriority WRITE setPriority NOTIFY priorityChanged)
    Q_PROPERTY(QString argument READ getArgument WRITE setArgument NOTIFY argumentChanged)
    Q_PROPERTY(bool haveIcon READ getHaveIcon NOTIFY iconChanged)
    Q_PROPERTY(QString iconUrl READ getIconUrl WRITE setIconUrl NOTIFY iconChanged)
    Q_PROPERTY(QString appName READ getAppName WRITE setAppName NOTIFY appNameChanged)
    Q_PROPERTY(EntryType entry READ getEntryType WRITE setEntryType NOTIFY entryTypeChanged)
    Q_PROPERTY(QString dbusArg READ getDbusArg WRITE setDbusArg NOTIFY dbusArgChanged)
    enum EntryType
    {
        LaunchApp = 0,
        XTermCommand = 1,
        DBusService = 2
    } entry;

    PowerMenuEntryCreator() : entry(LaunchApp), priority(500) {}
    PowerMenuEntryCreator(EntryType e) : entry(e), priority(500) {}

    Q_INVOKABLE inline int refreshSystemUI() { return system("killall systemui"); }
    Q_INVOKABLE inline bool removeEntry(QString file) { return QFile::remove(file); }
   // Q_INVOKABLE bool scanEntry(QString file);
    Q_INVOKABLE bool createEntry();
    //Q_INVOKABLE inline void setData(desktopDescriptor d) {appName = d.getAppName(); desktopName = d.getDesktop(); iconUrl = d.getIcon(); }
    Q_INVOKABLE inline void setData(QString an, QString dn, QString iu) {appName = an; argument = dn; iconUrl = iu;}
    static inline QString stripFilename(const QString filename) { return QFileInfo(filename).completeBaseName(); }

protected:
    int priority;
    QString argument, appName, // name visible to user and name of .desktop
            iconUrl, dbusArg;
    QXmlStreamWriter& writeLaunchAppCallback(QXmlStreamWriter& w);
    QXmlStreamWriter& writeXTermCommandCallback(QXmlStreamWriter& w);
    QXmlStreamWriter& writeDBusServiceCallback(QXmlStreamWriter& w);
    QString entryFileName() const;

    inline int getPriority() const {return priority;}
    inline QString getArgument() const {return argument;}
    inline bool getHaveIcon() const {return !iconUrl.isEmpty();}
    inline QString getIconUrl() const { return iconUrl; }
    inline QString getAppName() const {return appName; }
    inline EntryType getEntryType() const { return entry; }
    inline QString getDbusArg() const {return dbusArg; }

    inline void setPriority(int p) { priority = p; emit priorityChanged(p); }
    inline void setArgument(QString n) { argument = n; emit argumentChanged(n); }
    inline void setIconUrl(QString u) { iconUrl = u; emit iconChanged(u); }
    inline void setAppName(QString a) { appName = a; emit appNameChanged(a); }
    inline void setEntryType(EntryType type) { entry = type; emit entryTypeChanged(type); }
    inline void setDbusArg(QString ar) { dbusArg = ar; emit dbusArgChanged(ar); }

signals:
    void priorityChanged(int p);
    void argumentChanged(QString n);
    void iconChanged(QString i);
    void appNameChanged(QString a);
    void entryTypeChanged(EntryType e);
    void dbusArgChanged(QString arg);
    void malformedFile();
    void writeError();
};

#endif // POWERMENUENTRYCREATOR_H

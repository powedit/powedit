#ifndef DESKTOPFILESCANNER_H
#define DESKTOPFILESCANNER_H

#include <QObject>
#include <QMap>
#include <QStringList>
#include <QThread>

QString checkForIcon(const QString& i) throw(QString);

class desktopDescriptor : public QObject
{
    Q_OBJECT
    friend class DesktopFileScanner;
public:
    Q_PROPERTY(QString appName READ getAppName NOTIFY changed)
    Q_PROPERTY(QString icon READ getIcon NOTIFY changed)
    Q_PROPERTY(QString xiconpath READ getXiconpath NOTIFY changed)
    Q_PROPERTY(QString desktop READ getDesktop NOTIFY changed)
    Q_PROPERTY(QString iconAbs READ getIconAbs NOTIFY changed)
    QString findAbsoluteIconPath() const;
    desktopDescriptor (QString d, QString a, QString i, QString x) : desktop(d), appName(a), icon(i), xiconpath(x) {}
    void notifyChange() {emit changed();}
private:
    QString desktop, appName, icon, xiconpath;
public:
    QString getAppName() const { return appName; }
    QString getIcon() const { return icon; }
    QString getIconAbs() const { return findAbsoluteIconPath(); }
    QString getXiconpath() const { return xiconpath; }
    QString getDesktop() const { return desktop; }
signals:
    void changed();
};

class DesktopFileScanner : public QThread
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> appList READ getAppList NOTIFY appListChanged)
public:
    DesktopFileScanner(bool doScan = false) { if (doScan) scan(); }
    QMap<QString, QObject*> apps;
    void run();
    Q_INVOKABLE void scan() { start(QThread::HighPriority); }
protected:
    static const QString appShortcutDirName;
    QList<QObject*> getAppList() { return apps.values(); }
signals:
    void appListChanged();


};

#endif // DESKTOPFILESCANNER_H

# Add more folders to ship with the application, here

# Additional import path used to resolve QML modules in Creator's code model

folder_01.source = qml/components
folder_01.target = qml
folder_02.source = qml
folder_02.target = .
DEPLOYMENTFOLDERS = folder_01 folder_02

QML_IMPORT_PATH =

QT+= declarative xml
symbian:TARGET.UID3 = 0xE6DBEC64

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    PowerMenuEntryCreator.cpp \
    qmlapplicationviewer/qmlapplicationviewer.cpp \
    DesktopFileScanner.cpp \
    SystemUICfgScanner.cpp


OTHER_FILES += \
    qml/components/FileDialogSheet.qml \
    qml/components/FileDialogDialog.qml \
    qml/components/FileDialog.qml \
    qml/components/FileBrowserStyle.qml \
    qml/components/FileBrowser.qml \
    qmlapplicationviewer/qmlapplicationviewer.pri \
    qml/EnterDetailsPage.qml \
    powedit.desktop \
    qml/MainPag.qml \
    qml/main.qml

RESOURCES +=

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()

# enable booster
#CONFIG += qdeclarative-boostable
#QMAKE_CXXFLAGS += -fPIC -fvisibility=hidden -fvisibility-inlines-hidden
#QMAKE_LFLAGS += -pie -rdynamic

HEADERS += \
    PowerMenuEntryCreator.h \
    qmlapplicationviewer/qmlapplicationviewer.h \
    DesktopFileScanner.h \
    SystemUICfgScanner.h

#include "SystemUICfgScanner.h"
#include <QFileInfo>
#include <QtXml/QXmlStreamReader>
#include <QSettings>
#include <QDebug>

#include "DesktopFileScanner.h"


void SystemUICfgScanner::run()
{
    if ( file.isEmpty() )
    {
        emit noFileSpecified();
        return ;
    }

    QFileInfo info(file);
    if (! info.exists())
    {
        emit wrongFile(file);
        return ;
    }

    QFile f(file);
    f.open(QFile::ReadOnly);
    QXmlStreamReader reader(&f);

    descriptor.nameReadOnly = false; // we assume there's no <po>, and if is, we change this option

    while ( !reader.atEnd() && !reader.hasError() )
    {
        QXmlStreamReader::TokenType token = reader.readNext();
        if ( token == QXmlStreamReader::StartElement)
        {
            QString name = reader.name().toString();
            if ((name == "powerkeymenu") /*and...*/ ) continue;
            else if ( name == "po") descriptor.nameReadOnly = true; // it's translated so user'd better not mess with it
            else if ( name == "menuitem")
            {
                QXmlStreamAttributes attr = reader.attributes();
                if (attr.hasAttribute("priority"))
                {
                    descriptor.priority = attr.value("priority").toString().toInt();
                }
                if (attr.hasAttribute("name"))
                {
                    descriptor.name = attr.value("name").toString();
                }
                if (attr.hasAttribute("powedit"))
                {
                    descriptor.compliant = (attr.value("powedit") == "true");
                }
            }
            else if ( name == "callback")
            {
                QXmlStreamAttributes attr = reader.attributes();
                if (attr.hasAttribute("service"))
                {
                    if ( attr.value("service") == "com.nokia.HildonDesktop.AppMgr" )
                    {
                        bool malformed = false;
                        // sanity checks
                        if ( !attr.hasAttribute("path") || attr.value("path") != "/com/nokia/HildonDesktop/AppMgr") malformed = true;
                        if ( !attr.hasAttribute("interface") || attr.value("interface") != "com.nokia.HildonDesktop.AppMgr") malformed = true;
                        if ( !attr.hasAttribute("method") || attr.value("method") != "LaunchApplication") malformed = true;
                        if ( !attr.hasAttribute("bus") || attr.value("bus") != "session") malformed = true;

                        if (malformed) emit warning("The contents of the file aren't fully PowEdit-compliant.\n This isn't fatal, but you shouldn't proceed unless you know what you're doing.\n Please report it to the program developer");
                    }
                    else
                    {
                        emit error("Malformed entry");
                    }
                }
            }
            else if ( name == "argument")
            {
                descriptor.desktop = "/usr/share/applications/hildon/" + reader.readElementText() + ".desktop";
            }
            else if ( name == "icon")
            {
                descriptor.icon = reader.readElementText();
            }
        }
    }

    if (reader.hasError())
    {
        emit error("Parsing error");
        qDebug() << reader.errorString();
    }
    emit changed();
    if ( !descriptor.compliant ) emit error("Entry not compliant with PowEdit");
}

QString SystemUICfgScanner::findAbsoluteIconPath() const
{
    // maybe it's the same .desktop?

    QSettings * d = new QSettings("/usr/share/applications/hildon/" + descriptor.desktop, QSettings::IniFormat);

    if ( d->contains("X-Icon-path"))
    {
        QFileInfo info(d->value("X-Icon-path").toString() + '/' + descriptor.icon + ".png" );
        if (info.exists()) return info.absoluteFilePath();
    }
    else
    {
        try { return checkForIcon(descriptor.icon); }
        catch (QString) {} // just don't return ;)
    }

    return ""; // FIXME

}

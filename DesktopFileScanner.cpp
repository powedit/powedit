#include "DesktopFileScanner.h"
#include <QDir>
#include <QSettings>
#include <QDebug>
#include <QFileInfo>

QString checkForIcon(const QString& icon) throw(QString)
{
    QFileInfo info;
    QString file;
    QStringList dirs;
    dirs << "/usr/share/icons/hicolor/scalable/apps/" <<
            "/usr/share/icons/hicolor/64x64/apps/" <<
            "/usr/share/icons/hicolor/48x48/apps/" <<
            "/usr/share/icons/hicolor/scalable/hildon/" <<
            "/usr/share/icons/" <<
            "/usr/share/icons/hicolor/64x64/hildon/" <<
            "/usr/share/icons/hicolor/48x48/hildon/";
    foreach (const QString& dir, dirs)
    {
        file = dir + icon + ".png";
        info.setFile(file);
        if (info.exists())
            return file;
    }
    throw QString(); // if not found...
}

const QString DesktopFileScanner::appShortcutDirName("/usr/share/applications/hildon");

QString desktopDescriptor::findAbsoluteIconPath() const
{
    if (xiconpath.isEmpty())
    {
        try {return checkForIcon(icon);}
        catch (QString) {} // just exit
    }
    else
    {
        QString file;
        QFileInfo info;
        file = xiconpath + '/' + icon + ".png";
        info.setFile(file);
        if (info.exists())
            return info.absoluteFilePath();
    }
    return "/usr/share/icons/hicolor/64x64/hildon/tasklaunch_default_application.png";
}

void DesktopFileScanner::run()
{
    QDir appShortcutDir(appShortcutDirName);
    QStringList files = appShortcutDir.entryList(QStringList("*.desktop"), QDir::Files);
    desktopDescriptor * desc;
    foreach(const QString& file, files)
    {
        QSettings desktopFile(appShortcutDir.absoluteFilePath(file), QSettings::IniFormat);
        //qDebug() << "Parsing file " << desktopFile.fileName();
        desktopFile.beginGroup("Desktop Entry");
        if (desktopFile.value("Type").value<QString>() == "Daemon") continue;
        QString name = desktopFile.value("Name").value<QString>();
        // overrides

        if (name == "addr_ap_address_book") name = "Contacts";
        else if ( name == "ai_ap_name") name = "App. Manager";
        else if ( name == "back_ap_feature_name") name = "Backup";
        else if ( name == "cal_ap_name" ) name = "Calendar";
        else if ( name == "calc_ap_calculator") name = "Calculator";
        else if ( name == "camera_ap_camera" ) name = "Camera";
        else if ( name == "cema_ap_application_title") name = "Certificate Manager";
        else if ( name == "cloc_ap_name") name = "Clock";
        else if ( name == "copa_ap_cp_name" ) name = "Settings";
        else if ( name == "game_ap_blocks_name" ) name = "Blocks";
        else if ( name == "game_ap_chess_name" ) name = "Chess";
        else if ( name == "game_ap_mahjong_name" ) name = "Mahjong";
        else if ( name == "game_ap_marbles_name" ) name = "Marbles";
        else if ( name == "imag_ap_images") name = "Photos";

        QString xiconpath;
        if ( desktopFile.contains("X-Icon-path") )
        {
            xiconpath = desktopFile.value("X-Icon-path").value<QString>();
        }
        else
        {
            xiconpath = QString();
        }
        desc = new desktopDescriptor(
                    file,
                    name,
                    desktopFile.value("Icon").value<QString>(),
                    xiconpath);

        desktopFile.endGroup();
        apps.insert(name.toLower(), desc); // for
        desc->notifyChange();
    }
    //qDebug() << apps.values().count() << " .desktop files parsed ";
    emit appListChanged();
}

import QtQuick 1.1
import org.maemo.fremantle 1.0


Sheet
{
    id: manuallyDetailsSheet

    rejectButtonText: "Cancel"
    acceptButtonText: "OK"
    onAccepted:
    {
        if (iconName != "")
        {
            iconPath = ""
            iconName = iconNameField.text
            creator.iconUrl = iconName
            iconOnSwitch.checked = true
        }
        if (argument != "")
        {
            argument = argumentNameField.text
            creator.argument = argument
        }
    }

    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        Column
        {
            width:  parent.width
            spacing: 10
            Header
            {
                headertext: "Manual field editing (advanced)"
            }

            TextField
            {
                id: iconNameField
                text: iconName
                placeholderText: "Icon name (basename recommended, as in .desktop)"
                width: parent.width
            }
            Text
            {
                width: parent.width
                text: "You may enter a basename which is used by .desktop files in /usr/share/applications/hildon. Using manual editing you won't get the icon miniature."
                wrapMode: Text.WordWrap
                anchors
                {
                    left: parent.left
                    right: parent.right
                    leftMargin: 10
                    rightMargin: 10
                }
            }
            Item { height: 5; width: 1 } //spacer

            TextField
            {
                id: argumentNameField
                text: argument
                placeholderText: "Argument name"
                width: parent.width
            }
            Text
            {
                width: parent.width
                text: "This field may contain the location of .desktop if you're launching an app, the name of D-Bus service or an X-Term command, according to selected entry type"
                wrapMode: Text.WordWrap
                anchors
                {
                    left: parent.left
                    right: parent.right
                    leftMargin: 10
                    rightMargin: 10
                }
            }
        }
    }
}

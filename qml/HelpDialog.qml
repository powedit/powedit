import QtQuick 1.1
import org.maemo.fremantle 1.0

QueryDialog
{
    id: helpDialog
    acceptButtonText: qsTr("OK")
    message: qsTr("<h1>Help</h1><h3>Priority</h3><br />Priority sets the position of entry in the menu.<br />The higher the priority, the later in the menu the entry is put.<br /><br />The common entries have these priorities:<br /><br />Top in XTerm - 5<br />Reboot - 10<br />Switch off - 90<br />Secure device - 100<br />General profile - 200<br />Silent profile - 300<br />Offline/normal mode - 350<br />Phone - 400<br />CSSU Features, Swappolube - 401<br />Tablet/phone mode - 401<br />End current task - 600<br />Lock screen and keys - 700<br /><br /> <h3>D-Bus argument</h3><br />The argument can be only string type right now")
}

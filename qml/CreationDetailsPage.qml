import QtQuick 1.1
import org.maemo.fremantle 1.0
import org.maemo.extras 1.0
import PowEdit 1.0
import "components"

Page
{
    id: creationDetailsPage
    tools: creationDetailsPageTools

    property string iconPath: ""
    property string iconName: ""
    property alias appNameString: entryName.text
    property string argument: ""
    property string dbusArg: ""

    states: State
    {
        name: "iconOff"
        when: iconOnSwitch.checked == false
        PropertyChanges { target: selectIconRow; visible: false }
        PropertyChanges { target: iconMiniature; visible: (iconPath != "") }
    }

    /*transitions: Transition
    {
        reversible: true
        from: ""
        to: "iconOff"
        PropertyAnimation
        {
            properties: "x,y,visible"
            easing.type: Easing.InOutQuad
            from: selectIconRow
            property: "visible"
        }
    }*/

    function reset()
    {
        iconName = ""
        iconPath = ""
        appNameString = ""
        argument = ""
        prioritySlider.value = 500
        if ((iconPath == "") || (iconOnSwitch.checked == false)) iconMiniature.visible = false
    }

    Connections
    {
        target: cfgScanner
        onFinished:
        {
            console.log("cfgScanner finished, name is " + cfgScanner.name)
            iconName = cfgScanner.icon
            iconPath = cfgScanner.iconAbs
            if ((iconPath != "") && (iconOnSwitch.checked == true)) iconMiniature.visible = true
            appNameString = cfgScanner.name
            entryName.readOnly = cfgScanner.nameReadOnly
            argument = cfgScanner.desktop
        }
        onWarning:
        {
            //pageStack.pop()
            systemuicfgWarningDialog.infodetails = desc
            systemuicfgWarningDialog.open()
        }
        onError:
        {
            //pageStack.pop()
            systemuicfgErrorDialog.infodetails = desc
            systemuicfgErrorDialog.open()
        }
    }

    Loader
    {
        id: sheetLoader
    }

    Flickable
    {
        id: flickDetails
        anchors.fill: parent
        contentHeight: detailsColumn.height + 10 // some kinda margin
        flickableDirection: Flickable.VerticalFlick
        clip: true

        Column
        {
            id: detailsColumn
            width: parent.width
            spacing: 20
            Header
            {
                id: caption
                headertext: entryTypeDialog.model.get(entryTypeDialog.selectedIndex).name
            }
            TextField
            {
                id: entryName
                text: appNameString
                placeholderText: "Entry name"
                width: parent.width
            }

            Row
            {
                spacing: 10
                width: parent.width
                Label
                {
                    id: iconOnLabel
                    text: "  Icon enabled"
                    anchors.verticalCenter: iconOnSwitch.verticalCenter
                }
                Item {
                    // spacer
                    width: parent.width - 30 - iconOnLabel.width - iconOnSwitch.width // 20 for spacing, 10 for margin
                    height: 1
                }
                Switch
                {
                    id: iconOnSwitch
                    checked: true
                }
            }

            Row
            {
                spacing:  10
                width: parent.width
                id: selectIconRow
                Label
                {
                    id: selectedIconLabel
                    text: "  Selected icon: "
                    anchors.verticalCenter: selectIconButton.verticalCenter
                }
                Button
                {
                    id: selectIconButton
                    width: parent.width - 20 - selectedIconLabel.width
                    text: ( iconName == "" ) ? "No icon selected" : iconName
                    onClicked:
                    {
                        selectIconDialog.open()
                    }
                }
            }
            Image
            {
                id: iconMiniature
                source: iconPath
                height: 80
                width: 80
                sourceSize.height: 80
                sourceSize.width: 80
                anchors.horizontalCenter: parent.horizontalCenter
                visible: (iconPath != "")
            }
            Row
            {
                spacing:  10
                width: parent.width
                Label
                {
                    id: selectedAppLabel
                    text: "  Selected app: "
                    anchors.verticalCenter: selectAppButton.verticalCenter
                }
                Button
                {
                    id: selectAppButton
                    width: parent.width - 20 - selectedAppLabel.width
                    text: ( argument == "" ) ? "No app selected" : argument
                    onClicked:
                    {
                        selectDesktopDialog.open()
                    }
                }
                visible: (creator.entry == PowerMenuEntryCreator.LaunchApp)
            }

            Row
            {
                spacing:  10
                width: parent.width
                Label
                {
                    id: seletedServiceLabel
                    text: "  Selected service: "
                    anchors.verticalCenter: seletedServiceButton.verticalCenter
                }
                Button
                {
                    id: seletedServiceButton
                    width: parent.width - 20 - seletedServiceLabel.width
                    text: ( argument == "" ) ? "No service selected" : argument
                    onClicked:
                    {
                        selectServiceDialog.open()
                    }
                }
                visible: (creator.entry == PowerMenuEntryCreator.DBusService)
            }

            TextField
            {
                width: parent.width
                id: argTextField
                text: dbusArg
                placeholderText: "D-Bus service argument (blank = no argument)"
                visible: (creator.entry == PowerMenuEntryCreator.DBusService)
            }

            TextField
            {
                id: commandTextField
                width: parent.width
                text: argument
                placeholderText: "Command"
                visible: (creator.entry == PowerMenuEntryCreator.XTermCommand)
            }

            Column
            {
                spacing: 10
                width: parent.width
                Label
                {
                    id: sliderLabel
                    text: "Item priority:"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Slider
                {
                    id: prioritySlider
                    minimumValue: 0
                    maximumValue: 1000
                    value: 500
                    stepSize: 1
                    width: parent.width
                    valueIndicatorVisible: true
                    onValueChanged: creator.priority = value
                }
            }
            Button
            {
                id: gobutton
                width: parent.width - 20
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Go!"
                onClicked:
                {
                    if ( creator.entry == PowerMenuEntryCreator.XTermCommand )
                    {
                        argument = commandTextField.text
                        creator.argument = commandTextField.text // if it's a command
                    }
                    if (creator.entry == PowerMenuEntryCreator.DBusService)
                    {
                        dbusArg = argTextField.text
                        creator.dbusArg = dbusArg
                    }

                    if ( (entryName.text == "") || (argument == "") || ( (iconOnSwitch.checked == true) && (iconPath == "") ) )
                    {
                        fieldsEmpty.show()
                        return;
                    }

                    if (iconOnSwitch == false) creator.iconUrl = ""

                    creator.appName = entryName.text; // in case it changed


                    pageStack.pop()

                    if (creator.createEntry() == true)
                    {
                        creator.refreshSystemUI()
                        entryCreationOK.show()
                    }
                    else entryCreationFailed.show()

                }
            }
        }
    }

    InfoBanner
    {
        id: fieldsEmpty
        text: "Please fill all the fields!"
    }

    FileDialog
    {
        id: selectIconDialog
        folder: "/usr/share/icons/hicolor/"
        onAccepted:
        {
            creator.iconUrl = filePath
            iconPath = filePath
            if ((iconPath != "") && (iconOnSwitch.checked == true)) iconMiniature.visible = true
        }
    }

    FileDialog
    {
        id: selectDesktopDialog
        folder: "/usr/share/applications/hildon"
        onAccepted:
        {
            if (filePath.substring(0, folder.length) == folder)
            {
                console.log("File " + filePath + " is a correct .desktop file")
                argument = filePath
                creator.argument = filePath
            }
            else
            {
                console.log("File + " + filePath + " is not a correct .desktop file")
                selectDesktopDialog.open()
            }
        }
    }

    FileDialog
    {
        id: selectServiceDialog
        folder: "/usr/share/dbus-1/services/"
        onAccepted:
        {
            if (filePath.substring(0, folder.length) == folder)
            {
                console.log("File " + filePath + " is a correct .service file")
                argument = filePath
                creator.argument = filePath
            }
            else
            {
                console.log("File + " + filePath + " is not a correct .service file")
                selectServiceDialog.open()
            }
        }
    }

    QueryDialog
    {
        id: areYouSureClearDialog
        titleText: "Clear the fields"
        message: "Are you sure you want to clear all the fields"
        acceptButtonText: "OK"
        rejectButtonText: "Cancel"
        onAccepted: reset()
    }

    ToolBarLayout
    {
        id: creationDetailsPageTools
        anchors.centerIn: parent
        ToolIcon
        {
            platformIconId: "toolbar-back"
            onClicked:
            {
                pageStack.pop()
            }
        }

        ToolIcon
        {
            platformIconId: "toolbar-application"
            onClicked:
            {
                sheetLoader.source = "SelectAppSheet.qml"
                sheetLoader.item.open()
            }
            visible: (creator.entry == PowerMenuEntryCreator.LaunchApp)
        }

        ToolIcon
        {
            platformIconId: "input-clear"
            onClicked: areYouSureClearDialog.open()
        }

        ToolIcon
        {
            platformIconId: "toolbar-edit"
            onClicked:
            {
                sheetLoader.source = "ManuallyDetailsSheet.qml"
                sheetLoader.item.open()
            }
        }
    }

    BusyIndicator
    {
        id: inddetails
        running: true
        visible: false
        anchors.centerIn: parent
        platformStyle: BusyIndicatorStyle
        {
            size: "large"
        }
    }
}

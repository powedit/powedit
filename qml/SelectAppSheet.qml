import QtQuick 1.1
import org.maemo.fremantle 1.0


Sheet
{
    id: settingsSheet

    rejectButtonText: "Cancel"
    acceptButtonText: "Refresh"
    onAccepted:
    {
        scanner.scan()
        settingsSheet.open()
    }
    Component.onCompleted:
    {
        scanner.scan()
    }

    Connections
    {
        target: scanner
        onFinished: ind.visible = false
    }


    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        ListView
        {
            anchors.fill: parent
            clip: true

            model: scanner.appList
            spacing: 10
            delegate: Button
            {
                height: 80
                width: parent.parent.width
                onClicked:
                {
                    argument = modelData.desktop
                    iconPath = modelData.iconAbs
                    iconName = modelData.icon
                    appNameString = modelData.appName
                    creator.setData(modelData.appName, argument, iconName)
                    if ((iconPath != "") && (iconOnSwitch.checked == true)) iconMiniature.visible = true
                    settingsSheet.close()
                }

                Row
                {
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 15

                    Image
                    {
                        property int dimension: 64
                        id: icon
                        source: modelData.iconAbs
                        height: dimension
                        width: dimension
                        sourceSize.height: dimension
                        sourceSize.width: dimension
                    }

                    Text
                    {
                        id: name
                        text: modelData.appName
                        font.pixelSize: 32
                        anchors.verticalCenter: icon.verticalCenter
                    }
                }
            }
        }
    }
    BusyIndicator
    {
        id: ind
        running: true
        visible: true
        anchors.centerIn: parent
        platformStyle: BusyIndicatorStyle
        {
            size: "large"
        }
    }
}


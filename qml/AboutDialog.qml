import QtQuick 1.1
import org.maemo.fremantle 1.0

QueryDialog
{
    id: aboutDialog
    titleText: qsTr("PowEdit")
    acceptButtonText: qsTr("OK")
    message: qsTr("<br /> Copyright (C) 2012 Marcin Mielniczuk <br /><br /> If you want to support my work, please consider <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WUAUBSU3QQLNL'> donating </a> ")
}

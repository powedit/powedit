import QtQuick 1.1
import org.maemo.fremantle 1.0
import PowEdit 1.0

PageStackWindow
{
    //theme.colorScheme: "black"
    id: appWindow

    initialPage: selectActionTypePage

    SelectActionTypePage {id: selectActionTypePage}

    Loader
    {
        id: dialogLoader
    }

    ToolBarLayout
    {
        id: commonTools
        visible: true
        ToolIcon
        {
            platformIconId: "toolbar-view-menu";
            anchors.right: parent.right
            onClicked: (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
    }

    Menu
    {
        id: myMenu
        visualParent: pageStack
        MenuLayout
        {
            MenuItem
            {
                text: "Help"
                onClicked:
                {
                    dialogLoader.source = "HelpDialog.qml"
                    dialogLoader.item.open()
                }
            }
            MenuItem
            {
                text: "About"
                onClicked:
                {
                    dialogLoader.source = "AboutDialog.qml"
                    dialogLoader.item.open()
                }
            }
        }
    }

    PowerMenuEntryCreator { id: creator }
    DesktopFileScanner { id: scanner }
    SystemUICfgScanner
    {
        id: cfgScanner
    }

}

import QtQuick 1.1
import org.maemo.fremantle 1.0
import org.maemo.extras 1.0
import PowEdit 1.0
import "components"


Page
{
    id: mainPage
    tools: commonTools

    property int items: 3
    property int heightForSpacer: (height - caption.height - commonTools.height) / items
    property int heightForButton: heightForSpacer / 2
    property bool fileOK: false

    Connections
    {
        target: cfgScanner
        onNoFileSpecified: console.log("No file specified")
        onWrongFile: console.log("Wrong file: " + filename)
    }

    Loader
    {
        id: creationDetailsSheetLoader
        onLoaded:
        {
            console.log("creation details page loaded")
            //item.reset()
        }
    }
    Loader
    {
        id: notRunAsRootDialogLoader
    }
    Loader
    {
        id: fileDialogLoader
    }

    Component.onCompleted:
    {
        if (!runAsRoot)
        {
            notRunAsRootDialogLoader.sourceComponent = notRunAsRootDialog
            notRunAsRootDialogLoader.item.open()
        }
    }

    Column
    {
        spacing: 20
        width: parent.width
        Header
        {
            id: caption
            headertext: qsTr("Type of activity")
        }

        Item
        {
            width:  parent.width
            height: heightForSpacer
            Button
            {
                id: newEntryButton
                anchors.centerIn: parent
                text: "Create an entry"
                onClicked:
                {
                    entryTypeDialog.open()
                }
                Image
                {
                    source: "image://theme/icon-m-common-drilldown-arrow"
                    anchors
                    {
                        right: parent.right
                        rightMargin: 15
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        }
        Item
        {
            width:  parent.width
            height: heightForSpacer
            Button
            {
                id: editEntryButton
                anchors.centerIn: parent
                text: "Edit an entry"
                onClicked:
                {
                    fileDialogLoader.sourceComponent = selectConffileDialogEdit
                    fileDialogLoader.item.folder = "/etc/systemui"
                    fileDialogLoader.item.open() // forwarding control to the dialog

                    console.log("Wait patiently until we eat your dog... implement this, I mean! :D")
                }
                Image
                {
                    source: "image://theme/icon-m-common-drilldown-arrow"
                    anchors
                    {
                        right: parent.right
                        rightMargin: 15
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        }
        Item
        {
            width:  parent.width
            height: heightForSpacer
            Button
            {
                id: deleteEntryButton
                anchors.centerIn: parent

                text: qsTr("Delete an entry")
                onClicked:
                {
                    fileDialogLoader.sourceComponent = selectConffileDialogDelete
                    fileDialogLoader.item.folder = "/etc/systemui"
                    fileDialogLoader.item.open() // forwarding control to the dialog
                }
                Image
                {
                    source: "image://theme/icon-m-common-drilldown-arrow"
                    anchors
                    {
                        right: parent.right
                        rightMargin: 15
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        }
    }

    QueryDialog
    {
        id: wrongDirFileDialog
        titleText: qsTr("Warning")
        message: qsTr("The file you selected isn't located in /etc/systemui.<br />The power key menu won't change after editing this file.")
        acceptButtonText: qsTr("OK")
    }

    QueryDialog
    {
        id: wrongDirFileSystemFileDialog
        titleText: qsTr("Error")
        message: qsTr("The file you selected isn't located in /etc/systemui and is a system file.<br />Entry operation refused.<br />If you really want to brick your N900, do it yourself ;)")
        acceptButtonText: qsTr("OK")
    }

    QueryDialog
    {
        id: etcSystemUIFileDialog
        titleText: qsTr("Error")
        message: qsTr("The file you selected is an essential part of the system.<br />Entry operation refused.<br />If you really want to brick your N900, do it yourself ;)")
        acceptButtonText: qsTr("OK")
    }

    Component
    {
        id: notRunAsRootDialog
        QueryDialog
        {
            titleText: qsTr("Warning")
            message: qsTr("You don't run PowEdit as root. This may lead the program to malfunction.") // malfunction = permission problems
            acceptButtonText: qsTr("OK")
        }
    }

    InfoBanner
    {
        id: removalOKBanner
        text: "Removal of entry successful."
    }
    InfoBanner
    {
        id: removalFailedBanner
        text: "Removal of entry failed."
    }
    InfoBanner
    {
        id: entryCreationOK
        text: "Entry creation successful"
    }
    InfoBanner
    {
        property string details: ""
        id: entryCreationFailed
        text: "Entry creation failed"
        Connections
        {
            target: creator
            onWriteError: entryCreationFailed.details = ": write error"
            onMalformedFile: entryCreationFailed.details = ": malformed file"
        }
    }

    SelectionDialog
    {
        id: entryTypeDialog
        titleText: "Entry type"
        model: ListModel
        {
            ListElement { name: "Launch an application" }
            ListElement { name: "Run a command in xterm" }
            ListElement { name: "Launch a D-Bus service" }
        }
        onAccepted:
        {
            creator.entry = selectedIndex
            creationDetailsSheetLoader.source = "CreationDetailsPage.qml"
            creationDetailsSheetLoader.item.reset()
            pageStack.push(creationDetailsSheetLoader.item)
        }
    }

    QueryDialog
    {
        property string infodetails: ""
        id: systemuicfgErrorDialog
        titleText: "Error"
        message: infodetails
        rejectButtonText: "OK"
        onRejected: pageStack.pop()
    }

    QueryDialog
    {
        property string infodetails: ""
        id: systemuicfgWarningDialog
        titleText: "Warning"
        message: infodetails
        rejectButtonText: "OK"
    }

    Component
    {
        id: selectConffileDialogEdit
        FileDialog
        {
            property string homedir: "/home/user"
            folder: "/etc/systemui/"
            onAccepted:
            {


                console.log("filePath == " + filePath)
                if ( filePath.substring(0, folder.length) !=  folder ) // file not in SystemUI directory, but only warning
                {
                    if ( filePath.substring(0, homedir.length) != homedir ) // refuse to edit/delete system file
                    {
                        wrongDirFileSystemFileDialog.open()
                        return
                    }
                    else
                    {
                        wrongDirFileDialog.open()
                        return
                    }
                }

                else if ( filePath == "/etc/systemui/systemui.xml" )
                {
                    etcSystemUIFileDialog.open()
                    return
                }

                cfgScanner.file = filePath

                // files ok

                creationDetailsSheetLoader.source = "CreationDetailsPage.qml"
                // scan and after scanning load

                pageStack.push(creationDetailsSheetLoader.item)
                cfgScanner.scan()

            }
        }
    }


    Component
    {
        id: selectConffileDialogDelete
        FileDialog
        {
            property string homedir: "/home/user"
            folder: "/etc/systemui/"
            onAccepted:
            {
                console.log("filePath == " + filePath)
                if ( filePath.substring(0, folder.length) !=  folder ) // file not in SystemUI directory, but only warning
                {
                    if ( filePath.substring(0, homedir.length) != homedir ) // refuse to edit/delete system file
                    {
                        wrongDirFileSystemFileDialog.open()
                        return
                    }
                    else
                    {
                        wrongDirFileDialog.open()
                        return
                    }
                }

                else if ( filePath == "/etc/systemui/systemui.xml" )
                {
                    etcSystemUIFileDialog.open()
                    return
                }

                // files ok, now delete

                areYouSureDelDialog.thisFile = filePath
                areYouSureDelDialog.open() // forwarding control to the dialog
            }

            QueryDialog
            {
                property string thisFile
                id: areYouSureDelDialog
                titleText: qsTr("Are you sure you want to delete file " + thisFile )
                message: qsTr("This is an operation you cannot take back!")
                acceptButtonText: qsTr("Yes")
                rejectButtonText: qsTr("No")
                onAccepted:
                {
                    console.log("Removing " + thisFile)
                    if (creator.removeEntry(thisFile)) removalOKBanner.show()
                    else removalFailedBanner.show()
                    creator.refreshSystemUI()
                }
                onRejected: parent.filePath = ""
            }
        }
    }

}




//        Item
//        {
//            width: parent.width
//            height: heightForSpacer
//            Row
//            {
//                width: parent.width
//                spacing: 10
//                anchors.centerIn: parent
//                Label
//                {
//                    id: typelabel
//                    text: qsTr("  Entry type:")
//                    anchors.verticalCenter: selectEntryTypeButton.verticalCenter
//                }

//                Button
//                {
//                    id: selectEntryTypeButton
//                    width: parent.width - typelabel.width - 30 // - 10 for spacing, 2*-10 for margin
//                    text: entryTypeDialog.model.get(entryTypeDialog.selectedIndex).name
//                    onClicked: entryTypeDialog.open()
//                }
//            }
//        }
//        Item
//        {
//            width: parent.width
//            height: heightForSpacer
//            Button
//            {
//                id: specifyDetailsButton
//                text: "Specify details"
//                width: parent.width - 20
//                anchors.centerIn: parent
//                onClicked:
//                {
//                    enterDetailsSheetLoader.source = "EnterDetailsPage.qml"
//                    pageStack.push(enterDetailsSheetLoader.item)
//                }
//            }
//        }

//        Item
//        {
//            width: parent.width
//            height: heightForSpacer
//            Button
//            {
//                id: gobutton
//                width: parent.width - 20
//                anchors.centerIn: parent
//                text: "Go!"
//                onClicked:
//                {
//                    if (creator.createEntry() == true)
//                    {
//                        creator.refreshSystemUI()
//                        entryCreationOK.show()
//                    }
//                    else entryCreationFailed.show()
//                }
//            }
//        }
//    }

    /*SelectionDialog
    {
        id: entryTypeDialog
        titleText: "Entry type"
        selectedIndex: 0
        model: ListModel
        {
            ListElement { name: "Launch application" }
        }
        onAccepted: creator.setEntryType(selectedIndex)*/
    //}

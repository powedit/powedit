import QtQuick 1.1

Rectangle
{
    property string headertext
    id: maelyricaheader
    color: "#4591ff"
    height: 60
    width: parent.width
    Text
    {
        text: headertext
        color: "white"
        anchors
        {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: 30
        }
        font.pixelSize: 36
    }
}

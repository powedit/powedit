#include <QtGui/QApplication>
#include <QtDeclarative>
#include <unistd.h>

#include "PowerMenuEntryCreator.h"
#include "SystemUICfgScanner.h"
#include "DesktopFileScanner.h"
#include "qmlapplicationviewer/qmlapplicationviewer.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QmlApplicationViewer viewer;

    if ( getuid() != 0 ) viewer.rootContext()->setContextProperty("runAsRoot", QVariant(false));
    else viewer.rootContext()->setContextProperty("runAsRoot", QVariant(true));

    qmlRegisterType<PowerMenuEntryCreator>("PowEdit", 1, 0, "PowerMenuEntryCreator");
    qmlRegisterType<DesktopFileScanner>("PowEdit", 1, 0, "DesktopFileScanner");
    qmlRegisterType<SystemUICfgScanner>("PowEdit", 1, 0, "SystemUICfgScanner");

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/main.qml"));
    viewer.showFullScreen();

    return app.exec();
}

#ifndef SYSTEMUICFGSCANNER_H
#define SYSTEMUICFGSCANNER_H

#include <QObject>
#include <QThread>

class SystemUICfgScanner : public QThread
{
    Q_OBJECT
    Q_PROPERTY(QString file READ getFile WRITE setFile NOTIFY fileChanged)
    Q_PROPERTY(QString name READ getName NOTIFY changed)
    Q_PROPERTY(bool nameReadOnly READ getNameReadOnly NOTIFY changed)
    Q_PROPERTY(bool compliant READ getCompliant NOTIFY changed)
    Q_PROPERTY(QString icon READ getIcon NOTIFY changed)
    Q_PROPERTY(QString iconAbs READ getIconAbs NOTIFY changed)
    Q_PROPERTY(QString desktop READ getDesktop NOTIFY changed)
    Q_PROPERTY(int priority READ getPriority NOTIFY changed)
protected:
    struct descriptor_t
    {
        QString name, icon, desktop;
        int priority;
        bool nameReadOnly, compliant;
    } descriptor;

public:
    Q_INVOKABLE void scan() { start(QThread::LowPriority); }
    QString findAbsoluteIconPath() const;
protected:

    QString file;
    void run();

    inline QString getName() const { return descriptor.name; }
    inline bool getNameReadOnly() const { return descriptor.nameReadOnly; }
    inline bool getCompliant() const { return descriptor.compliant; }
    inline QString getIcon() const { return descriptor.icon; }
    inline QString getIconAbs() const { return findAbsoluteIconPath(); }
    inline QString getDesktop() const { return descriptor.desktop; }
    inline int getPriority() const { return descriptor.priority; }
    inline QString getFile() const { return file; }

    inline void setFile(QString f) {file = f; emit fileChanged();}
signals:
    void changed();
    void fileChanged();
    void noFileSpecified();
    void wrongFile(QString filename);
    void error(QString desc);
    void warning(QString desc);
};

#endif // SYSTEMUICFGSCANNER_H

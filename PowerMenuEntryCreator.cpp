#include "PowerMenuEntryCreator.h"
#include <QDebug>
#include <QSettings>

QXmlStreamWriter& PowerMenuEntryCreator::writeLaunchAppCallback(QXmlStreamWriter& w)
{

    /*QDomElement el = doc.createElement("callback");
    el.setAttribute("service", "com.nokia.HildonDesktop.AppMgr");
    el.setAttribute("path", "/com/nokia/HildonDesktop/AppMgr");
    el.setAttribute("interface", "com.nokia.HildonDesktop.AppMgr");
    el.setAttribute("method", "LaunchApplication");
    el.setAttribute("bus", "session");
    el.setAttribute("autostart", true);
    return el;*/


    QXmlStreamAttributes atr;
    atr.append("service", "com.nokia.HildonDesktop.AppMgr");
    atr.append("path", "/com/nokia/HildonDesktop/AppMgr");
    atr.append("interface", "com.nokia.HildonDesktop.AppMgr");
    atr.append("method", "LaunchApplication");
    atr.append("bus", "session");
    atr.append("autostart", "true");

    w.writeStartElement("callback");
    w.writeAttributes(atr);

    w.writeStartElement("argument");
    w.writeAttribute("type", "string");
    w.writeCharacters(stripFilename(argument));

    w.writeEndElement(); // argument

    w.writeEndElement(); // callback

    return w;
}

QXmlStreamWriter& PowerMenuEntryCreator::writeXTermCommandCallback(QXmlStreamWriter &w)
{

    QXmlStreamAttributes atr;
    atr.append("service", "com.nokia.xterm");
    atr.append("path", "/com/nokia/xterm");
    atr.append("interface", "com.nokia.xterm");
    atr.append("method", "run_command");
    atr.append("bus", "session");
    atr.append("autostart", "true");

    w.writeStartElement("callback");
    w.writeAttributes(atr);

    w.writeStartElement("argument");
    w.writeAttribute("type", "string");
    w.writeCharacters(argument);

    w.writeEndElement(); // argument

    w.writeEndElement(); // callback

    return w;
}

QXmlStreamWriter& PowerMenuEntryCreator::writeDBusServiceCallback(QXmlStreamWriter &w)
{
    QSettings servicefile(argument, QSettings::IniFormat);
    servicefile.beginGroup("D-Bus Service");
    if (! servicefile.contains("Name"))
    {
        emit malformedFile();
        throw QChar();
    }

    QString service = servicefile.value("Name").toString();
    QString service_path = service;

    QXmlStreamAttributes atr;
    atr.append("service", service);
    atr.append("path", "/" + service_path.replace('.', '/'));
    atr.append("interface", service);
    atr.append("method", "top_application");
    atr.append("bus", "session");
    atr.append("autostart", "true");

    w.writeStartElement("callback");
    w.writeAttributes(atr);

    if (!dbusArg.isEmpty())
    {
        w.writeStartElement("argument");
        w.writeAttribute("type", "string");
        w.writeCharacters(dbusArg);
        w.writeEndElement(); // argument
    }

    w.writeEndElement(); // callback

    return w;
}

bool PowerMenuEntryCreator::createEntry()
{
    QFile file(entryFileName());
    file.open(QFile::WriteOnly | QFile::Truncate);
    qDebug() << "Saving entry to " << entryFileName();
    QXmlStreamWriter writer(&file);
    writer.setAutoFormatting(true);
    writer.setCodec("UTF-8");

    //writer.writeStartDocument();

    writer.writeStartElement("powerkeymenu");
    writer.writeAttribute("path", "/");

    writer.writeStartElement("menuitem");
    writer.writeAttribute("priority", QString::number(priority));
    writer.writeAttribute("name", appName);
    writer.writeAttribute("powedit", "true");

    if (getHaveIcon() == true)
    {
        writer.writeTextElement("icon", iconUrl);
    }

    try
    {
        switch (entry)
        {
        case LaunchApp: writeLaunchAppCallback(writer); break;
        case XTermCommand: writeXTermCommandCallback(writer); break;
        case DBusService: writeDBusServiceCallback(writer); break;
        }
    }
    catch (QChar)
    {
        //file.remove();
        return false;
    }


    writer.writeEndElement(); // menuitem

    writer.writeEndElement(); // powerkeymenu

    file.close();
    if (file.error() != QFile::NoError)
    {
        //file.remove();
        emit writeError();
        return false;
    }
    else return true;
}

QString PowerMenuEntryCreator::entryFileName() const
{
    return ("/etc/systemui/" + appName.trimmed().toLatin1().replace(' ', '_') + ".xml");
}
